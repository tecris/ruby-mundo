#!/usr/bin/ruby

class Person

  def initialize(name)
    @name = name
    @age = nil
  end

  attr_accessor :age

  def info
    puts "Name: #{@name}, age: #{@age}"
  end

  def Person.method_a
    puts "Class method - class name"
  end

  def self.method_b
    puts "Class method - self style"
  end
end

class Employee < Person

  def initialize(name, salary)
    super(name)
    @salary = salary
  end
end

Person.method_a
Person.method_b

ion = Person.new("Ion")
ion.age = 47
ion.info
