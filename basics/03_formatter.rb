#!/usr/bin/ruby


formatter = "%{first} %{second} %{third} %{quatro}"

puts formatter % {first: 1, second: 2, third: 3, quatro: 4}
puts formatter % {first: "first", second: "second", third: "third", quatro: "fourth"}
puts formatter % {first: true, second: false, third: true, quatro: false}
puts formatter % {first: formatter, second: formatter, third: formatter, quatro: formatter}

puts formatter % {
  first: "UNO",
  second: "DUE",
  third: "TRES",
  quatro: "QUATRO"
}

puts formatter % {
  second: "DUE",
  third: "TRES",
  first: "UNO",
  quatro: "QUATRO"
}

puts formatter % {
  first: "QUATRO",
  second: "DUE",
  third: "TRES",
  quatro: "UNO"
}
