#!/usr/bin/ruby

class Person
  def name
    @name
  end
  def name=(str)
    @name = str
  end
end

person = Person.new
puts "Person.name: #{person.name}"
person.name = 'Cristo'
puts "Person.name: #{person.name}"

class Country
  attr_accessor :capital
end

country = Country.new
puts "Country.capital: #{country.capital}"
country.capital = 'Bucharest'
puts "Country.capital: #{country.capital}"

class City
  def initialize(name = 'Bucuresti', population)
    @name = name
    @population = population
  end
  attr_reader :name, :population
  attr_writer :area

  attr_writer :area

  def to_s
    puts "Name: #{@name}, Area: #{@area} square km, Population: #{@population}"
  end
end

city = City.new(1_900_000)
puts "city.name: #{city.name}"
city.area = 228
city.to_s

city = City.new('Berlin', 3_500_000)
city.area = 892
puts "city.name: #{city.name}"
puts "city.population: #{city.population}"
city.to_s
