#!/usr/bin/ruby


if ARGV.first.nil?
  abort("Usage ./09_files_write.rb file")
end

filename = ARGV.first

puts "About to delete file #{filename}"
puts "CTRL-C to cancel"
puts "RETURN to delete"

$stdin.gets

puts "Opening file ..."
target = open(filename, 'w')

puts "Truncating file ... "
target.truncate(0)


print "line: "
line1 = $stdin.gets.chomp
print "line: "
line2 = $stdin.gets.chomp
print "line: "
line3 = $stdin.gets.chomp

target.write(line1)
target.write("\n")
target.write(line2)
target.write("\n")
target.write(line3)
target.write("\n")

target.close
