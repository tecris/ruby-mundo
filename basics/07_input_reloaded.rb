#!/usr/bin/ruby


if ARGV.first.nil?
  abort("Usage ./07_input_reloaded.rb user")
end

user_name = ARGV.first
prompt = '> '

puts "Hi #{user_name}"
puts "#{user_name} feedback please"
puts prompt

likes = $stdin.gets.chomp

puts """
Your feedback: #{likes}
"""
