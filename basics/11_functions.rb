#!/usr/bin/ruby


def method_a(var1, var2)
  x = var1
  y = var2
  puts "arg-1: #{x}, arg-2: #{y}"
end

def method_b(var1, var2)
  x = var1
  y = var2
  return x
end

def method_c(var1, var2)
  x = var1
  y = var2
  return x, y
end

method_a("Matrix", "Movie")
puts
puts method_b("From", "Other")
puts
puts method_c("Dark", "Black")
