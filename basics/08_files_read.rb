#!/usr/bin/ruby


if ARGV.first.nil?
  abort("Usage ./08_files_read.rb file_to_read")
end

filename = ARGV.first

txt = open(filename)

puts "Reading file #{filename}"
print txt.read

print "Enter file name, again ..."
file_second = $stdin.gets.chomp
txt_second = open(file_second)
print txt_second.read
