#!/usr/bin/ruby


countries = {
  'Austria' => 'AT',
  'Bulgaria' => 'BG',
  'France' => 'FR',
  'Germany' => 'DE',
  'Poland' => 'PL',
  'Romania' => 'RO'
}

capitals = {
  'AT' => 'Vienna',
  'BG' => 'Sofia',
  'FR' => 'Paris',
  'DE' => 'Berlin',
  'PL' => 'Warsaw',
  'RO' => 'Bucharest'
}

countries.each do |item|
  puts "Entry: #{item}"
end

puts

countries.each do |country, code|
  puts "Country: #{country}, capital: #{capitals[code]}"
end

puts

countries.each do |item|
  puts "Country: #{item[0]}, capital: #{capitals[item[1]]}"
end
