#!/usr/bin/ruby


the_count = [1, 2, 3]

the_count.each do |my_no|
  puts "my number #{my_no}"
end

(0..3).each { |e| puts "This is #{e}"  }

my_array = []

(4..7).each do |i|
  my_array.push(i)
end

my_array.each { |e| puts "my_array: #{e}"  }

for my_numero in my_array
  puts "this is number #{my_numero}"
end

i = 6
my_other_array = []

while i < 9
   my_other_array.push(i)
   i += 1;
 end


my_other_array.each { |e| puts "my_other_array: #{e}"  }
