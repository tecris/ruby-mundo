#!/usr/bin/ruby


def method_a(*args)
  for i in 0...args.length
    puts "arg-#{i}: #{args[i]}"
  end
end

def method_b(*args)
  x, y = args
end

def method_c(var1, var2)
  x = var1
  y = var2
end

def method_d(var1, var2)
  y = var2
  x = var1
end

method_a("Matrix", "Reloaded")
puts
puts method_b("Cloud", "Atlas")
puts
puts method_c("Golden", "Eye")
puts
puts method_d("From", "Other")
