#!/usr/bin/ruby


if ARGV[0].nil? or ARGV[1].nil?
  abort("Usage ./10_file_copy.rb file-source file-destination")
end

from_file, to_file = ARGV

puts "Copy from #{from_file} to #{to_file}"

in_file = open(from_file)
indata = in_file.read

out_file = open(to_file, 'w')
out_file.write(indata)

out_file.close
in_file.close
